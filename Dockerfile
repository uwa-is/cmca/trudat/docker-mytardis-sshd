FROM registry.gitlab.com/codemerc/docker-sshd/sshd:python-2.7.15-slim-stretch

RUN usermod --login mydata user \
 && usermod --move-home --home /home/mydata mydata

COPY ./Dockerfile /Dockerfile
